; Think Theatre make file
api = 2
core = 7.x

; Modules

projects[domain][version] = "3.11"
projects[domain][subdir] = "contrib"

projects[domaincontext][version] = "1.0-alpha1"
projects[domaincontext][subdir] = "contrib"

projects[domain_views][version] = 1.5
projects[domain_views][subdir] = contrib
projects[domain_views][type] = module

projects[flexslider][version] = "2.x-dev"
projects[flexslider][subdir] = "contrib"

projects[file_entity][version] = "2.0-unstable7"
projects[file_entity][subdir] = "contrib"

projects[fitvids][version] = "1.17"
projects[fitvids][subdir] = "contrib"

projects[media][version] = "2.0-alpha4"
projects[media][subdir] = "contrib"

projects[media_vimeo][version] = "2.0"
projects[media_vimeo][subdir] = "contrib"

projects[media_youtube][version] = "2.0-rc5"
projects[media_youtube][subdir] = "contrib"

projects[modernizr][version] = "3.0-beta3"
projects[modernizr][subdir] = "contrib"

projects[bootstrap][type] = theme
projects[bootstrap][version] = 3.x-dev
projects[bootstrap][subdir] = contrib

projects[bootstrap_business][type] = theme
projects[bootstrap_business][version] = 1.1
projects[bootstrap_business][subdir] = contrib
